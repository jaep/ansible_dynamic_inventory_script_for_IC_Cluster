#!/usr/bin/env python3
import argparse
import json
import sys

import requests
from decouple import config

LIST_URL = config(
    "LIST_URL",
    default="https://ic-toolbox.epfl.ch/inventory/deployment/servers/ansible",
)
HOST_URL = config(
    "HOST_URL",
    default="https://ic-toolbox.epfl.ch/inventory/deployment/servers/ansible/{host}",
)


def get_list(pretty=False) -> str:
    response = requests.get(LIST_URL)
    if response.status_code != 200:
        raise Exception("Error getting list")

    return (
        json.dumps(response.json(), indent=2) if pretty else json.dumps(response.json())
    )


def get_host_details(host: str, pretty=False) -> str:
    response = requests.get(HOST_URL.format(host=host))
    if response.status_code != 200:
        raise Exception("Error getting host details")

    return (
        json.dumps(response.json(), indent=2) if pretty else json.dumps(response.json())
    )


if __name__ == "__main__":
    with open("main.log", "a") as f:
        f.write(f"{sys.argv}\n")

    arg_parser = argparse.ArgumentParser(description=__doc__, prog=__file__)
    arg_parser.add_argument(
        "--pretty", action="store_true", default=False, help="Pretty print JSON"
    )
    mandatory_options = arg_parser.add_mutually_exclusive_group()
    mandatory_options.add_argument(
        "--list",
        action="store_true",
        help="Show JSON of all managed hosts",
    )
    mandatory_options.add_argument(
        "--host", action="store", help="Display vars related to the host"
    )

    try:
        args = arg_parser.parse_args()
        if args.host:
            print(get_host_details(args.host, args.pretty))
        elif args.list:
            print(get_list(args.pretty))
        else:
            raise ValueError("Expecting either --host $HOSTNAME or --list")
    except ValueError:
        raise
