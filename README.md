# Sample use of a dynamic inventory script for Ansible in the context of IC-Cluster

This repository provides a sample dynamic inventory script that can be used with Ansible to manage the IC-Cluster nodes.

## Requirements

This script requires the following Python modules:

- ansible
- requests
- python-decouple

## Installation

As usual, it is better to use a virtual environment to install the required modules:

```bash
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install --upgrade pip
$ pip install -r requirements.txt
```

## Configuration

Two environment variables can be used to change the default configuration:

| Variable | Default value                                                          | Description                                                     |
| -------- | ---------------------------------------------------------------------- | --------------------------------------------------------------- |
| LIST_URL | https://ic-toolbox.epfl.ch/inventory/deployment/servers/ansible        | URL used by ansible-inventory to retrieve the list of nodes     |
| NODE_URL | https://ic-toolbox.epfl.ch/inventory/deployment/servers/ansible/{node} | URL used by ansible-inventory to retrieve the details of a node |

## Usage

The script can be used as a dynamic inventory script for Ansible:

```bash
ansible-inventory -i iccluster.py --list
```

If you want to use it in playbook, you can use the following configuration:

```bash
ansible-playbook -i inventory/main.py playbook.yml
```

**Sample playbook**

```yaml
---
- name: sample playbook using a dynamic inventory
  hosts: all
  gather_facts: false
  connection: local
  tasks:
    - name: debug
      debug:
        var: server_type
      when: server_type=='ICCANode'
```
